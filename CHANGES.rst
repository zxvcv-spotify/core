Changelog
=========

0.0.4 (2021-12-12)
------------------
- Package name change to spotify.core.
- Move util to spotify.util package.

0.0.3 (2021-12-10)
------------------
- Restructurization.
- Fixes in implementation.

0.0.2 (2021-12-09)
------------------
- First implementation.

0.0.1 (2021-12-07)
------------------
- Initial commit.
