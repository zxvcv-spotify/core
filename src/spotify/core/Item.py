
class Item():
    """ """

    _SP = None

    def __init__(self, details: dict):
        self.id = details["id"]

    def __eq__(self, other):
        return (self.id == other.id)

    def __str__(self):
        return str(self.id)
