from .Item import Item

class User(Item):
    def __init__(self, details: dict):
        super().__init__(details)

    @classmethod
    def get_current(cls):
        return User(cls._SP.current_user())

    @classmethod
    def get_by_name(cls, name:str):
        return User(cls._SP.user(name))
